import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

/* MODULES
---------------------------------------------*/
const appStore = {
	namespaced: true,
	state: {
		brand: {
			name: '',
			logo: ''
		},
		brands: [],
		isLoading: true,
		error: null
	},
	mutations: {
		updateStore(state, payload) {
			const prevStoreData = JSON.parse(JSON.stringify(state));
			const newStoreData = {
				...prevStoreData,
				...payload
			};

			Object.assign(state, newStoreData);
		}
	},
	getters: {
		getState: (state) => (key) => {
			if (key) {
				return state[key];
			}
			return state;
		}
	}
};

const editorStore = {
	namespaced: true,
	state: {
		isMoving: true,
		components: {
			turbo: false,
			airvent: false,
			luggage: false,
			fourDoor: false,
			strips: false,
			roofExtention: false,
			spoiler: false,
			wheel: {
				solid: false,
				sidewall: false,
				colored: false
			}
		},
		options: {
			engineBay: ['turbo', 'airvent', 'wheel'],
			passengerArea: ['luggage', 'doorStrip', 'door'],
			trunkSpace: ['roofExtention', 'spoiler', 'wheel']
		},
		selectedArea: ''
	},
	mutations: {
		setEditorStateByBrand(state, brand) {
			const brandName = brand.toUpperCase();
			const isBMW = brandName === 'BMW';
			const isVW = brandName === 'VW';
			const isAudi = brandName === 'AUDI';

			const prevStoreData = JSON.parse(JSON.stringify(state));
			const newStoreData = {
				...prevStoreData,
				components: {
					...prevStoreData.components,
					airvent: isBMW,
					fourDoor: !isVW,
					spoiler: isAudi,
					wheel: {
						solid: isVW,
						sidewall: isBMW,
						colored: isAudi
					}
				}
			}

			Object.assign(state, newStoreData);
		},
		handleCarPartsSelection(state, selection) {

			const prevStoreData = JSON.parse(JSON.stringify(state));

			if (selection.name === 'wheel') {

				const newStoreData = {
					...prevStoreData,
					components: {
						...prevStoreData.components,
						wheel: {
							...prevStoreData.components.wheel,
							[selection.type]: selection.value
						}
					}
				};

				Object.assign(state, newStoreData);

			} else {

				const newStoreData = {
					...prevStoreData,
					components: {
						...prevStoreData.components,
						[selection.name]: selection.value
					}
				};

				Object.assign(state, newStoreData);
			}
		},
		handleSelectedArea(state, area) {

			const prevStoreData = JSON.parse(JSON.stringify(state));
			const newStoreData = {
				...prevStoreData,
				selectedArea: area
			}

			Object.assign(state, newStoreData);
		}
	},
	getters: {
		getState: (state) => (key) => {
			return state[key];
		}
	}
};

/* STORE
---------------------------------------------*/
export default new Vuex.Store({
	modules: {
		app: appStore,
		editor: editorStore
	}
})
