import Vue from 'vue'
import App from './00_global/app/scripts';
// import router from './router'
import store from './00_global/store'

Vue.config.productionTip = false

new Vue({
	// router,
	store,
	render: h => h(App)
}).$mount('#app')
